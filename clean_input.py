import pandas as pd
import csv
import clean_outputs

# read input.csv into pandas.DataFrame
l = []
with open("data/inputs/input.csv", 'r') as file:
    reader = csv.reader(file)
    for row in reader:
        l.append(pd.Series(row))
        
inp_df = pd.DataFrame(l[1:])
inp_df.columns = l[0]

merged_df = clean_outputs.clean_output_dfs(inp_df, id_suffix="@id", sep=".", missing_val='-') 