# -*- coding: utf-8 -*-
"""
Created on Sun Jan 30 09:13:39 2022

@author: miceas
"""

import pandas as pd
import distutils.util
import csv
import clean_outputs

# --- test 1a
# removes empty rows and only keeps subsetted columns that contain data 
# from pandas.Dataframe
inp_df = pd.DataFrame({
        "a" : ["keep","drop"],
        "b" : ["drop","drop"]
        })
    
out_df = pd.DataFrame({
        "a" : ["keep"]
        })

cleaned_df = clean_outputs.clean_df(inp_df, colnames=["a"], missing_val="drop")

assert(cleaned_df.equals(out_df))

# --- test 1b
# removes empty rows and keeps all columns if subset not provided
inp_df = pd.DataFrame({
        "a" : ["keep","drop","drop"],
        "b" : ["drop","keep","drop"]
        })
    
out_df = pd.DataFrame({
        "a" : ["keep","drop"],
        "b" : ["drop","keep"]
        })

cleaned_df = clean_outputs.clean_df(inp_df, missing_val="drop")

assert(cleaned_df.equals(out_df))

# --- test 2
inp_df_a = pd.DataFrame({
        "a.@id" : [1,2,3]
        })
inp_df_b = pd.DataFrame({
        "b.a.@id" : [1,2,3]
        })
inp_dfs = {"a" : inp_df_a,
           "b" : inp_df_b}
    
out_df = pd.DataFrame({
        "b.a.@id" : [1,2,3],
        "a.@id" : [1,2,3]
        })

merged_df = clean_outputs.merge_2_dfs(inp_dfs,
                                      "b", 
                                      "a", 
                                      ".", 
                                      "@id")

assert(merged_df.equals(out_df))

# --- test 3a
# iterates through linked keys to merge all dataframes together
# next_df_ids order means that simple stepping through dfs from source to 
# target would fail, instead method allows target to look into source 
# e.g. first merge is a -> c
#      ac cannot look into dataframe b
#      however merge_all_dfs searches for links in source and target
#      therefore next step is b -> ac
inp_df_a = pd.DataFrame({
        "a.@id" : [1,2,3],
        "a.c.@id" : ["a","b","c"]
        }).astype("object")
inp_df_b = pd.DataFrame({
        "b.a.@id" : [1,2,3]
        }).astype("object")
inp_df_c = pd.DataFrame({
        "c.@id" : ["a","b","c"]
        }).astype("object")

inp_dfs = {"a" : inp_df_a,
           "b" : inp_df_b,
           "c" : inp_df_c}

out_df = pd.DataFrame({"b.a.@id" : [1,2,3],
                       "a.@id" : [1,2,3],
                       "a.c.@id" : ["a","b","c"],
                       "c.@id" : ["a","b","c"]}).astype("object")

merged_df = clean_outputs.merge_all_dfs(ind_dfs=inp_dfs, 
                                        next_df_ids=[["a","c","@id"],
                                                     ["b","a","@id"]], 
                                        df_ids_set=set(["b","a","c"]),
                                        id_suffix="@id", 
                                        sep=".") 

assert(merged_df.equals(out_df))

# --- test 3b
# script produces two dataframes that match expectations where not all dataframes
# are linked
inp_df_a = pd.DataFrame({
        "a.@id" : [1,2,3]
        }).astype("object")
inp_df_b = pd.DataFrame({
        "b.a.@id" : [1,2,3]
        }).astype("object")
inp_df_y = pd.DataFrame({
        "y.@id" : [1,2,3]
        }).astype("object")
inp_df_z = pd.DataFrame({
        "z.y.@id" : [1,2,3]
        }).astype("object")
inp_dfs = {"a" : inp_df_a,
           "b" : inp_df_b,
           "y" : inp_df_y,
           "z" : inp_df_z}

out_df1 = pd.DataFrame({
        "b.a.@id" : [1,2,3],
        "a.@id" : [1,2,3]
        }).astype("object")

out_df2 = pd.DataFrame({
        "z.y.@id" : [1,2,3],
        "y.@id" : [1,2,3]
        }).astype("object")

merged_df = clean_outputs.merge_all_dfs(ind_dfs=inp_dfs, 
                                        next_df_ids=[["b","a","@id"],
                                                     ["z","y","@id"]], 
                                        df_ids_set=set(["b","a","z","y"]),
                                        id_suffix="@id", 
                                        sep=".") 

assert(merged_df[0].equals(out_df1))
assert(merged_df[1].equals(out_df2))

# --- test 4a
# script converts input CSV to output CSV

# read input.csv into pandas.DataFrame
l = []
with open("data/inputs/input.csv", 'r') as file:
    reader = csv.reader(file)
    for row in reader:
        l.append(pd.Series(row))
        
inp_df = pd.DataFrame(l[1:])
inp_df.columns = l[0]

merged_df = clean_outputs.clean_output_dfs(inp_df, id_suffix="@id", sep=".", missing_val='-') 

# whilst clean_output_dfs reconstructs output.csv there are some differences
# in the formatting. It may be more appropriate to implement these changes
# in the code that was used to generate output.csv, e.g. floats in output.csv
# are often rounded compared to floats in input.csv
# However, for the sake of this task format_merged_df compares the product
# of clean_output_dfs and output.csv to match their formats before comparing
# where edits in clean_output_dfs were deemed inappropriate 
def format_merged_df(merged_df, out_df):
    """
    Compare the formats of the output from clean_output_dfs and output.csv
    to correct known format mismatches between the two objects
    
    Parameters
    ----------
    merged_df : pandas.DataFrame
        dataframe returned by clean_outputs.clean_output_dfs()
        
    out_df : pandas.DataFrame
        output.csv used to test script functionality

    Returns
    -------
    merged_df : pandas.DataFrame
        merged_df with modified formatting to account for known mismatches between
        clean_outputs.clean_output_dfs() output and output.csv
        
    out_df : pandas.DataFrame
        out_df with inferred dtypes
    """
        
    # rearrange columns and rows to output
    merged_df = merged_df[out_df.columns]
    
    ref_len = 0
    for col in merged_df.columns:
        if(len(set(merged_df[col])) > ref_len):
            final_col = col
            ref_len = len(set(merged_df[col]))
    
    # sort rows
    merged_df = merged_df.sort_values(by=[final_col]).reset_index(drop=True)
    out_df = out_df.sort_values(by=[final_col]).reset_index(drop=True)

    # merged_df and out_df dtypes are all object
    # infer integer and bool dtypes 
    for c in merged_df.columns:
        try: # convert objects to floats and integers 
            merged_df[c] = pd.to_numeric(merged_df[c])
            
            # pandas.to_numeric rounds final 0 in floats with trailing 0s
            # to 1, therefore floats are identified by the presence of "." in
            # a column numeric column and converted using float()
            if any([len(str(v).split(".")) > 1 for v in merged_df[c]]):
                out_df[c] = [float(v) for v in out_df[c]] 
            
            else:
                # if not a float, use pandas.to_numeric - no known integer conversion issues
                out_df[c] = pd.to_numeric(out_df[c])
            
        except:
            try: # convert objects to bools
                merged_df[c] = pd.Series([bool(distutils.util.strtobool(x)) for x in merged_df[c]])
                out_df[c] = pd.Series([bool(distutils.util.strtobool(x)) for x in out_df[c]])
            except:
                pass
            
    # floats are presented in inp_df to n d.p.
    # however, the values are not always presented to the same number of
    # d.p. in output.csv
    # identify number of d.p. in output.csv and round merged_df to make
    # number of d.p. match
    for c in merged_df.columns:
        if merged_df[c].dtype in ["float32", "float64"]:
            # 
            # remove trailing zeroes as pandas converts last 0 to 1 for floats
            out_df[c] = [float(v) for v in out_df[c]]
            #out_df[c] = pd.to_numeric(out_df[c])
            
            get_tuple = lambda x: (len(x.split('.')[1]))
            out = out_df[c].astype(str).apply(get_tuple)

            merged_df[c] = pd.Series([round(v, ndp) for ndp, v in zip(out, merged_df[c])])
        
    return merged_df, out_df

# read output.csv and reformat it and clean_outputs.clean_output_dfs to account
# for known formatting differences
    
l = []
with open("data/outputs/output.csv", 'r') as file:
    reader = csv.reader(file)
    for row in reader:
        l.append(pd.Series(row))
        
out_df = pd.DataFrame(l[1:])
out_df.columns = l[0]
        
merged_df, out_df = format_merged_df(merged_df, out_df)
            
assert(merged_df.equals(out_df))

# --- test 4b
# script produces two dataframes that match expectations where not all dataframes
# are linked
inp_df = pd.DataFrame({
        "a.@id" : [1,2,3,"-","-","-","-","-","-","-","-","-"],
        "b.a.@id" : ["-","-","-",1,2,3,"-","-","-","-","-","-"],
        "y.@id" : ["-","-","-","-","-","-",1,2,3,"-","-","-"],
        "z.y.@id" : ["-","-","-","-","-","-","-","-","-",1,2,3]
        })
    
out_df1 = pd.DataFrame({
        "b.a.@id" : [1,2,3],
        "a.@id" : [1,2,3]
        }).astype("object")

out_df2 = pd.DataFrame({
        "z.y.@id" : [1,2,3],
        "y.@id" : [1,2,3]
        }).astype("object")

merged_df = clean_outputs.clean_output_dfs(inp_df, id_suffix="@id", sep=".", missing_val='-') 

assert(merged_df[0].equals(out_df1))
assert(merged_df[1].equals(out_df2))