from .merge_2_dfs import merge_2_dfs

def merge_all_dfs(ind_dfs, next_df_ids, df_ids_set, sep, id_suffix):
    """
    Merge all dataframes together using id columns
    
    Parameters
    ----------
    ind_dfs : dict
        dictionary of dataframes to be merged
        
    next_df_ids : string
        name of columns separated by sep used as keys to look into related dataframes
        
    df_ids_set : string
        set of dataframe identifiers (first element of column name)
        
    sep : string
        separator used to separate elements of column names
        
    id_suffix : string
        suffix used to label id columns

    Returns
    -------
    merged_df_l : pandas.DataFrame
        dataframe of merged dataframes, or list of dataframes of merged dataframes - if 
        multiple merged dataframes returned
    """
    
    merged_dfs = set() # list of dataframes already already merged
    rem_next_df_ids = next_df_ids # remaining next_df_ids
    
    # start by merging first pair 
    next_df_id = rem_next_df_ids[0]
    
    # while all dataframes ids in merged_df or no more next dataframes ids to run 
    # (useful if not all dataframes are connnected)
    finished = False 
    merged_df_l = [] # if not enough keys to fully connected input dataframe, 
                     # return list of individual merged dataframes
    while not finished:
            
        source_df_id = next_df_id[0]
        target_df_id = next_df_id[1]
        
        merged_df = merge_2_dfs(ind_dfs, 
                                source_df_id, 
                                target_df_id, 
                                sep, 
                                id_suffix)
    
        # overwrite ind_dfs so merge_2_dfs() reads merged_df 
        ind_dfs[source_df_id] = merged_df
        ind_dfs[target_df_id] = merged_df
        
        # remove remaining next dataframe ids that are already in merged_df
        merged_dfs.update([x.split(sep)[0] for x in merged_df.columns])
        
        rem_next_df_ids = [next_df_id for next_df_id in rem_next_df_ids if
         not (next_df_id[0] in merged_dfs and next_df_id[1] in merged_dfs)]
    
        # update source and target df ids
        # while all dataframe ids in merged_df or no more next dataframe ids to run 
        # useful if not all dataframes are connnected
        if merged_dfs != df_ids_set or len(rem_next_df_ids) > 0:
            
            # next df ids that are connected to merged_df
            connected_n_df_ids = [next_df_id for next_df_id in rem_next_df_ids if 
                                 source_df_id in next_df_id or target_df_id in next_df_id]
            
            if len(connected_n_df_ids) > 0:
                next_df_id = connected_n_df_ids[0]
            else:
                next_df_id = rem_next_df_ids[0]
                merged_df_l.append(merged_df) # if no connections to merged_df exist, 
                                              # but there are still connected dataframes 
                                              # add current merged_df to list and start next
                
        else:
            finished = True
    merged_df_l.append(merged_df)
            
    # export dataframe/s
    for i, df in enumerate(merged_df_l, 1):
        fn = "data/outputs/merged_df_" + str(i) + ".csv"
        df.to_csv(fn, index=False)
        
    # if only one dataframe, return a dataframe. Otherwise return list of dataframes
    if len(merged_df_l) == 1:
        merged_df_l = merged_df_l[0]
        
    return merged_df_l

