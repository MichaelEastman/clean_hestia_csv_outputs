def clean_df(df, colnames=None, missing_val="-"):
    """
    Clean data dataframe (remove empty columns and rows)
    
    Parameters
    ----------
    df : pandas.DataFrame
        dataframe to be cleaned
        
    colnames : string
        names of columns to be kept
        
    missing_val : string
        value used to identify missing data

    Returns
    -------
    df : pandas.DataFrame
        dataframe without empty columns and rows
    """
    
    if colnames is None:
        colnames = df.columns

    df = df[colnames]
    
    # remove rows that are all missing_val
    missing_idx = [idx for idx in df.index if sum(df.loc[idx] == missing_val) == df.shape[1]]
    df = df.drop(missing_idx)
    
    return df
