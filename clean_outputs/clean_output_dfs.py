from .clean_df import clean_df
from . merge_all_dfs import merge_all_dfs

def clean_output_dfs(inp_df, sep=".", id_suffix="@id", missing_val="-"):
    """
    Data downloaded from Hestia as CSVs returns single data nodes per rows,
    even when nodes are related. 
    
    This script takes a CSV output and row merges it to put all related data
    on the same line.
    
    Parameters
    ----------
    inp_df : pandas.DataFrame
        dataframe returned from Hestia CSV download request
    
    sep : string
        separator used to separate elements of column names
    
    id_suffix : string
        suffix used to label id columns
        
    missing_val : string
        value used to identify missing data
        
    Returns
    -------
    merged_df_l : pandas.DataFrame
        dataframe of merged dataframes, or list of dataframes of merged dataframes - if 
        multiple merged dataframes returned
    """
    
    #inp_df.replace("-", np.nan, inplace=True) 
    
    # column names indicate which dataframe the information comes from, as well as
    # information about what the column information contains
    # split column names so dataframes and id columns can be identified
    split_colnames = [x.split(sep) for x in inp_df.columns]
    
    df_ids = [x[0] for x in split_colnames]
    df_ids_set = set(df_ids)

    # there is also a defaultSource.@id column that may be referring to the source dataframe
    # currently ignored as there are enough connections without it to connect the dataframes
    
    # 3 element column names that end in id_suffix are keys used to look from
    # element one dataframe into element two dataframe
    # extract these so they can be used to iterate through all possible dataframe
    # keys for row merging
    next_df_ids = [x for x in split_colnames if len(x) > 2 and x[-2] in set(df_ids)]
    
    # separate inp_df into separate dataframes for merging
    # first link dataframe key with column names from corresponding dataframe
    colnames_ind_df = dict(zip(
            df_ids_set,
            [[colname for colname in inp_df.columns if colname.split(".")[0] == df_id]
            for df_id in df_ids_set]))
    
    # then extract data from inp_df that comes from corresponding dataframe
    ind_dfs = {k: clean_df(df=inp_df, colnames=v, missing_val=missing_val) for k, v in colnames_ind_df.items()}
    
    # merge individual dataframes together using shared ids
    merged_df_l = merge_all_dfs(ind_dfs, 
                                next_df_ids, 
                                df_ids_set, 
                                sep, 
                                id_suffix)
    
    return merged_df_l

