import pandas as pd

def merge_2_dfs(ind_dfs, source_df_id, target_df_id, sep, id_suffix):
    """
    Merge two data dataframes together using to id columns
    
    Parameters
    ----------
    ind_dfs : dict
        dictionary of dataframes to be merged
        
    source_df_id : string
        key of source dataframe to be merged
        
    target_df_id : string
        key of target dataframe to be merged
        
    sep : string
        separator used to separate elements of column names
        
    id_suffix : string
        suffix used to label id columns

    Returns
    -------
    merged_df : pandas.DataFrame
        dataframe of two merged dataframes
    """
    
    # column name of shared id in source and target dataframes
    source_df_id_colname = sep.join([source_df_id, target_df_id, id_suffix])
    target_df_id_colname = sep.join([target_df_id, id_suffix])
    
    source_df = ind_dfs[source_df_id]
    target_df = ind_dfs[target_df_id]
    
    
    merged_df = pd.merge(source_df, 
                         target_df, 
                         left_on=source_df_id_colname, 
                         right_on=target_df_id_colname)
    
    return merged_df

